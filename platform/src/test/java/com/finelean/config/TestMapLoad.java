package com.finelean.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by sso on 3/19/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:test-map-context.xml")
public class TestMapLoad {

    @Resource
    private Map<Integer, String> analyzableEventTypes;

    @Test
    public void testMapLoad() {
        Assert.isTrue(analyzableEventTypes.containsKey(7));
    }
}
