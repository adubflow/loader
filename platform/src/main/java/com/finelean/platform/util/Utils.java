/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.util;

import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 * @author sso
 */
public final class Utils {

    private static final Logger logger = Logger.getLogger(Utils.class);

    private Utils() {

    }

    /**
     * Extracts from dalskjfd/aslfdj/2013-01-02
     *
     * @param input the input
     * @return The last 10 characters in the string
     */
    public static final String extractDateString(final String input) {

        String[] parts = input.toString().split("/");

        return parts[parts.length - 1];
    }

    public static final String convertStackTrace(Throwable t) {
        String retVal = null;
        try (StringWriter sw = new StringWriter()) {
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            retVal = sw.toString();
        } catch (Exception e) {
            logger.error("Unable to convert stack trace: ", e);
        }

        return retVal;

    }

}
