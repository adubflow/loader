/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.util;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * http://www.javacodegeeks.com/2010/07/java-best-practices-dateformat-in.html
 *
 * @author sso
 */
public final class DateUtils {

    private static final Logger logger = Logger.getLogger(Utils.class);

    private DateUtils() {

    }

    private static ThreadLocal<DateFormat> df = new ThreadLocalDateFormat("yyyy-MM-dd");

    private static ThreadLocal<DateFormat> longFormat = new ThreadLocalDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public static DateFormat getLongFormat() {
        return longFormat.get();
    }

    public static Date convert(String dateString) {
        try {
            return df.get().parse(dateString);
        } catch (Exception e) {
            logger.error("unable to parse: ", e);
        }

        return null;
    }

    private static class ThreadLocalDateFormat extends ThreadLocal<DateFormat> {

        private final String format;

        public ThreadLocalDateFormat(final String format) {
            this.format = format;
        }

        @Override
        public DateFormat get() {
            return super.get();
        }

        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat(format);
        }

        @Override
        public void remove() {
            super.remove();
        }

        @Override
        public void set(DateFormat value) {
            super.set(value);
        }
    };
}
