/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.config;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * This class exposes the Logger for setting the level.
 * The user can connect jconsole to the server process and
 * set the log level.
 *
 * @author sso
 */
@Component
@ManagedResource(objectName="com.finelean.loader:group=platform,name=Log4jConfigurer",
                 description="loaderapp log4j configuration")
public class Log4jConfigurer {

    @ManagedOperation(description="set log level")
    @ManagedOperationParameters({
            @ManagedOperationParameter(name="category",
                                       description = "set category"),
            @ManagedOperationParameter(name="level",
                                       description="set level")
    })
    public final void setLogLevel(final String category, final String level) {

        if (!StringUtils.isEmpty(category) && !StringUtils.isEmpty(level)) {

            Logger logger = Logger.getLogger(category);

            if (logger != null) {
                logger.setLevel(Level.toLevel(level.toUpperCase()));
            }
        }
    }

}
