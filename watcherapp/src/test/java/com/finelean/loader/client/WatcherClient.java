package com.finelean.loader.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.*;
import org.apache.log4j.Logger;

import java.io.InputStreamReader;
import java.net.URI;
import java.util.Scanner;

/**
 * Created by sso on 3/21/14.
 */
public class WatcherClient {

    private static final Logger logger = Logger.getLogger(WatcherClient.class);

    private final URI uri;

    private final Bootstrap bootstrap;

    private final WatcherClientHandler handler;

    private final EventLoopGroup group;

    private Channel channel;

    public WatcherClient(URI uri) throws Exception {

        this.uri = uri;

        group = new NioEventLoopGroup();

        bootstrap = new Bootstrap();
        String protocol = uri.getScheme();

        if (!"ws".equals(protocol)) {
            throw new IllegalArgumentException("Unsupported protocol: " + protocol);
        }

        HttpHeaders customHeaders = new DefaultHttpHeaders();
        customHeaders.add("MyHeader", "MyValue");

        handler = new WatcherClientHandler(WebSocketClientHandshakerFactory.newHandshaker(
                                uri, WebSocketVersion.V13, null, false, customHeaders));

        bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast("http-codec", new HttpClientCodec());
                            pipeline.addLast("aggregator", new HttpObjectAggregator(8192));
                            pipeline.addLast("ws-handler", handler);
                        }
                    });

        channel = bootstrap.connect(uri.getHost(), uri.getPort()).sync().channel();
        handler.handshakeFuture().sync();

    }

    public void run(final String message) throws Exception {

        switch (message) {
            case "close":
                channel.writeAndFlush(new CloseWebSocketFrame());
                break;
            case "ping" :
                channel.writeAndFlush(new PingWebSocketFrame(Unpooled.copiedBuffer(new byte[] {1,2,3})));
                break;
            default:
                channel.writeAndFlush(new TextWebSocketFrame(message));
                break;
        }

    }

    public void shutdown() {
        group.shutdownGracefully();
    }

    public static void main(String[] args) throws Exception {
        WatcherClient c = null;
        try {
            c = new WatcherClient(new URI("ws://localhost:8080/websocket"));
            Scanner scanner = new Scanner(new InputStreamReader(System.in));
            while (scanner.hasNext()) {
                String input = scanner.next();
                if (input.equals("quit")) {
                    break;
                } else {
                    c.run(input);
                }
            }

        } finally {
            if (c != null) {
                c.shutdown();
            }
        }
    }
}
