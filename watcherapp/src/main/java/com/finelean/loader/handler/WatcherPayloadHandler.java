/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finelean.loader.domain.event.EventByFiveMinuteKey;
import com.finelean.loader.service.WatcherService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.*;
import static io.netty.handler.codec.http.HttpMethod.*;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;

/**
 * This class defines how the payload is handled.
 *
 * @author sso
 */
@Component
@Qualifier("payloadHandler")
@ChannelHandler.Sharable
public class WatcherPayloadHandler extends SimpleChannelInboundHandler<Object> {

    private static final Logger logger = Logger.getLogger(WatcherPayloadHandler.class);

    private static final String WEBSOCKET_PATH = "/websocket";

    @Autowired
    private WatcherService watcherService;

    @Autowired
    private ObjectMapper objectMapper;

    private WebSocketServerHandshaker webSocketServerHandshaker;

    @Override
    public void channelRead0(ChannelHandlerContext context, Object msg) throws Exception {

        if (msg instanceof FullHttpRequest) {
            handleHttpRequest(context, (FullHttpRequest)msg);
        } else if (msg instanceof WebSocketFrame) {
            handleWebSocketFrame(context, (WebSocketFrame) msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext context) {
        context.flush();
    }

    private void handleHttpRequest(ChannelHandlerContext context, FullHttpRequest request) throws Exception {

        if (!request.getDecoderResult().isSuccess()) {
            sendHttpResponse(context, request, new DefaultFullHttpResponse(HTTP_1_1, BAD_REQUEST));
            return;
        }

        if (request.getMethod() != GET) {
            sendHttpResponse(context, request, new DefaultFullHttpResponse(HTTP_1_1, FORBIDDEN));
            return;
        }

        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(
                getWebSocketLocation(request), null, false);

        webSocketServerHandshaker = wsFactory.newHandshaker(request);
        if (webSocketServerHandshaker != null) {
            webSocketServerHandshaker.handshake(context.channel(), request);
        } else {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(context.channel());
        }

    }

    private void handleWebSocketFrame(ChannelHandlerContext context, WebSocketFrame frame) {

        final Channel channel = context.channel();

        if (frame instanceof CloseWebSocketFrame) {
            webSocketServerHandshaker.close(channel, (CloseWebSocketFrame) frame.retain());
            return;
        }

        if (frame instanceof PingWebSocketFrame) {
            channel.write(new PongWebSocketFrame(frame.content().retain()));
            return;
        }

        if (frame instanceof TextWebSocketFrame) {
            String request = ((TextWebSocketFrame) frame).text();
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("%s received %s", channel, request));
            }

            boolean eventOverThreshold = false;

            EventByFiveMinuteKey eventByFiveMinuteKey;
            try {
                eventByFiveMinuteKey = objectMapper.readValue(request, EventByFiveMinuteKey.class);
                Date startTime = eventByFiveMinuteKey.getEventTimeSlot();

                Date endTime = addMinutes(startTime, 5);

                eventOverThreshold = watcherService.isEventOverThreshold(eventByFiveMinuteKey, startTime, endTime);

            }  catch (Exception e) {
                logger.error("Unable to parse input.  ", e);
            }

            channel.write(new TextWebSocketFrame( "{\"result\": \"" + eventOverThreshold + "\"}" ));
            return;
        }

        if (frame instanceof BinaryWebSocketFrame) {
            throw new UnsupportedOperationException(
                    String.format("%s frame types to be supported", frame.getClass().getName()));
        } else {
            throw new UnsupportedOperationException(
                    String.format("%s frame types to be supported", frame.getClass().getName()));
        }
    }

    private static void sendHttpResponse(ChannelHandlerContext context,
                                         FullHttpRequest request,
                                         FullHttpResponse response) {
        if (response.getStatus().code() != OK.code()) {
            ByteBuf buf = Unpooled.copiedBuffer(response.getStatus().toString(), CharsetUtil.UTF_8);
            response.content().writeBytes(buf);
            buf.release();
            setContentLength(response, response.content().readableBytes());
        }

        // Send the response and close the connection if necessary.
        ChannelFuture f = context.channel().writeAndFlush(response);
        if (!isKeepAlive(request) || response.getStatus().code() != OK.code()) {
            f.addListener(ChannelFutureListener.CLOSE);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) throws Exception {
        cause.printStackTrace();
        context.close();
    }

    private static String getWebSocketLocation(FullHttpRequest request) {
        return "ws://" + request.headers().get(HOST) + WEBSOCKET_PATH;
    }

    private Date addMinutes(final Date startTime, int minutesToAdd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        calendar.add(Calendar.MINUTE, minutesToAdd);
        return calendar.getTime();
    }

}
