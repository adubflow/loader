/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.handler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * This class initializes the channel.
 * The qualifier name is important to be generic.
 * We share the same NettyServerInitializer for all netty processes.
 *
 * @author sso
 * @see com.finelean.core.server.config.NettyServerInitializer
 */
@Component
@Qualifier("channelInitializer")
public class WatcherChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Autowired
    SimpleChannelInboundHandler<Object> payloadHandler;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {

        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("codec-http", new HttpServerCodec());

        pipeline.addLast("aggregator", new HttpObjectAggregator(65536));

        pipeline.addLast("handler", payloadHandler);
    }
}
