/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.server.config;

import com.finelean.core.server.config.NettyServerInitializer;
import com.finelean.loader.domain.event.EventCriteria;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for loading the configuration and
 * initializes the spring beans.
 *
 * @author sso
 */
public class WatcherServerInitializer extends NettyServerInitializer {

    private static final Logger logger = Logger.getLogger(WatcherServerInitializer.class);

    @Bean(name = "eventCriteriaMap")
    public Map<Integer, EventCriteria> getEventCriteriaMap() {

        Map<Integer, EventCriteria> map;
        try {
            map = objectMapper.readValue(
                    ClassLoader.getSystemResourceAsStream("eventCriteriaMap.json"), HashMap.class);
        } catch (Exception e) {
            map = new HashMap<>();
        }
        return map;
    }


}
