package com.finelean.loader.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.net.InetSocketAddress;

/**
 * This is a server class which bootstraps the channel and
 * starts listening at a certain port.
 *
 * @author sso
 */
@Component
public class WatcherServer {

    @Autowired
    private InetSocketAddress inetSocketAddress;

    @Autowired
    private ServerBootstrap serverBootstrap;

    private Channel serverChannel;

    public void run() throws Exception {

        serverChannel = serverBootstrap.bind(inetSocketAddress)
                .sync()
                .channel()
                .closeFuture()
                .sync().channel();
    }

    @PreDestroy
    public void stop() {

        serverChannel.close();

    }

}
