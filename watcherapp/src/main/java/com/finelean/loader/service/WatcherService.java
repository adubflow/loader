package com.finelean.loader.service;

import com.finelean.loader.domain.event.EventByFiveMinute;
import com.finelean.loader.domain.event.EventByFiveMinuteKey;
import com.finelean.loader.domain.event.EventCriteria;
import com.finelean.loader.repositories.cassandra.EventByFiveMinuteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by sso on 3/20/14.
 */
@Component
public class WatcherService {

    @Resource
    private Map<Integer, EventCriteria> eventCriteriaMap;

    @Autowired
    private EventByFiveMinuteRepository eventByFiveMinuteRepository;

    public boolean isEventOverThreshold(final EventByFiveMinuteKey eventByFiveMinuteKey,
                                        final Date startTime,
                                        final Date endTime) {

        boolean retVal = false;

        List<EventByFiveMinute> list =
                eventByFiveMinuteRepository.findByEventTimeBetween(eventByFiveMinuteKey, startTime, endTime);

        if (list != null && !list.isEmpty()) {

            Integer eventId;
            for (EventByFiveMinute eventByFiveMinute : list) {
                eventId = eventByFiveMinute.getEventByFiveMinuteKey().getEventId();
                EventCriteria eventCriteria = eventCriteriaMap.get(eventId);
                if (eventCriteria != null) {
                    if (eventByFiveMinute.getCounter() > eventCriteria.getThreshold()) {
                        retVal = true;
                        break;
                    }
                }
            }
        }

        return retVal;
    }


    //@Scheduled(initialDelay=10000, fixedRate = 60000)
    private void scanForFrequentEvent() {


    }

}
