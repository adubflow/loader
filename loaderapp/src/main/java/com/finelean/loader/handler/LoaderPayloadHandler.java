/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.handler;

import com.finelean.loader.services.strategy.DateBasedDirectoryVisitStrategy;
import com.finelean.loader.services.strategy.DirectoryVisitStrategy;
import com.finelean.loader.task.LoaderTask;
import com.finelean.platform.util.DateUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static io.netty.buffer.Unpooled.*;

import java.io.IOException;
import java.net.URI;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;

/**
 * This class serves as the entry point to trigger the ETL process
 * to Cassandra repository.
 *
 * An initial version of supporting REST with Netty 4.
 * Would have been easier for developer if we do it with Play framework.
 *
 * @author sso
 */
@Component
@Qualifier("payloadHandler")
@ChannelHandler.Sharable
public class LoaderPayloadHandler extends SimpleChannelInboundHandler<Object> {

    private static final Logger logger = Logger.getLogger(LoaderPayloadHandler.class);

    @Autowired
    private ApplicationContext applicationContext;

    private @Value("${data.root.path}") String rootPath;

    private @Value("${core.pool.size}") int corePoolSize;

    private @Value("${max.pool.size}") int maxPoolSize;

    private ThreadPoolTaskExecutor loaderTaskExecutor;

    @PostConstruct
    private void init() {
        loaderTaskExecutor = new ThreadPoolTaskExecutor();
        loaderTaskExecutor.setCorePoolSize(corePoolSize);
        loaderTaskExecutor.setMaxPoolSize(maxPoolSize);
        loaderTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
    }

    /**
     * Determines what to execute by examining the URI.
     *
     * @param context the channel handler context.
     * @param httpObject the http message.
     * @throws Exception
     */
    @Override
    public void channelRead0(ChannelHandlerContext context, Object httpObject) throws Exception {

        if (httpObject instanceof HttpRequest) {

            HttpRequest request = (HttpRequest) httpObject;

            URI uri = new URI(request.getUri());

            final String uriPath = uri.getPath();

            StringBuilder responseContent = new StringBuilder();

            if (!uriPath.startsWith("/loader/v1")) {

                responseContent.append("Wrong URL.  \n");
                responseContent.append("Either /loader/v1/load/path/{date} \n");
                responseContent.append("Or     /loader/v1/bulkload/from/{fromDate}/to/{toDate}");
                writeResponse(context.channel(), request, responseContent);

            }

            String paths[] = uriPath.split("/");

            if (!rootPath.endsWith("/")) {
                rootPath += "/";
            }

            if (uriPath.contains("/load/path/")) {

                String date = paths[paths.length - 1];

                load(date);

            } else if (uriPath.contains("/bulkload/")) {

                int lastIndex = paths.length - 1;

                String endDate = paths[lastIndex];

                String startDate = paths[lastIndex - 2];

                load(startDate, endDate);

            }

            responseContent.append("ok\n");

            writeResponse(context.channel(), request, responseContent);

        }


    }

    private void load(String date) {

        LoaderTask loaderTask = (LoaderTask) applicationContext.getBean(LoaderTask.NAME, rootPath + date, null);

        loaderTaskExecutor.submit(loaderTask);
    }

    private void load(String startDate, String endDate) {

        LoaderTask loaderTask;

        Date f = DateUtils.convert(startDate);
        Date t = DateUtils.convert(endDate);

        DirectoryVisitStrategy directoryVisitStrategy = new DateBasedDirectoryVisitStrategy(f, t);

        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(rootPath))) {

            for (java.nio.file.Path path : directoryStream) {

                loaderTask = (LoaderTask) applicationContext.getBean(LoaderTask.NAME,
                        path.toAbsolutePath().toString(),
                        directoryVisitStrategy);

                loaderTaskExecutor.submit(loaderTask);
            }

        } catch (IOException ioe) {
            logger.error(ioe);
        }

    }

    private void writeResponse(Channel channel,
                               HttpRequest request,
                               StringBuilder responseContent) {

        ByteBuf buf = copiedBuffer(responseContent.toString(), CharsetUtil.UTF_8);

        responseContent.setLength(0);

        final HttpHeaders requestHeaders = request.headers();
        final String connection = requestHeaders.get(CONNECTION);
        boolean close = HttpHeaders.Values.CLOSE.equalsIgnoreCase(connection)
                || request.getProtocolVersion().equals(HttpVersion.HTTP_1_0)
                && !HttpHeaders.Values.KEEP_ALIVE.equalsIgnoreCase(connection);

        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1, HttpResponseStatus.OK, buf);

        HttpHeaders responseHeaders = response.headers();
        responseHeaders.set(CONTENT_TYPE, "text/plain; charset=UTF-8");

        if (!close) {
            responseHeaders.set(CONTENT_LENGTH, buf.readableBytes());
        }

        // no cookies to write
        ChannelFuture channelFuture = channel.writeAndFlush(response);
        if (close) {
            channelFuture.addListener(ChannelFutureListener.CLOSE);
        }
    }

}
