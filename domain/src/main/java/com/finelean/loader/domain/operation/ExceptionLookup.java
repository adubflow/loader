/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.operation;

import com.finelean.loader.domain.common.MetricKey;
import com.finelean.loader.domain.common.MetricKeyable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * This class represents exception lookup in the cassandra table.
 *
 * @author sso
 */
@Entity(name="exception_lookup")
@IdClass(MetricKey.class)
public class ExceptionLookup implements MetricKeyable, Serializable {

    private static final long serialVersionUID = 100000006L;

    @EmbeddedId
    protected MetricKey metricKey;

    @Override
    public MetricKey getMetricKey() {
        return metricKey;
    }

    public void setMetricKey(MetricKey metricKey) {
        this.metricKey = metricKey;
    }

    @Column(name="exception_stack")
    private String exceptionStack;

    public String getExceptionStack() {
        return exceptionStack;
    }

    public void setExceptionStack(String exceptionStack) {
        this.exceptionStack = exceptionStack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExceptionLookup that = (ExceptionLookup) o;

        if (!exceptionStack.equals(that.exceptionStack)) return false;
        if (!metricKey.equals(that.metricKey)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = metricKey.hashCode();
        result = 31 * result + exceptionStack.hashCode();
        return result;
    }
}

