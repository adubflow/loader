/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.device;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * This class represents the composite primary key for the os version.
 *
 * @author sso
 */
public class OSVersionKey implements Serializable {

    private static final long serialVersionUID = 100000003L;

    @Column(name="os_version")
    private String osVersion;

    @Column
    private String make;

    @Column
    private String model;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OSVersionKey that = (OSVersionKey) o;

        if (!make.equals(that.make)) return false;
        if (!model.equals(that.model)) return false;
        if (!osVersion.equals(that.osVersion)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = osVersion.hashCode();
        result = 31 * result + make.hashCode();
        result = 31 * result + model.hashCode();
        return result;
    }
}
