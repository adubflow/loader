/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.domain.device;

import javax.persistence.Column;
import java.io.*;

/**
 * This class represents the composite primary key for
 * the device meta information.
 *
 * @author sso
 */
public class DeviceMetaInfoKey implements Serializable {

    private static final long serialVersionUID = 100000001L;

    @Column
    private String id;

    @Column
    private String make;

    @Column
    private String model;

    @Column
    private String carrier;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceMetaInfoKey that = (DeviceMetaInfoKey) o;

        if (!carrier.equals(that.carrier)) return false;
        if (!id.equals(that.id)) return false;
        if (!make.equals(that.make)) return false;
        if (!model.equals(that.model)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + make.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + carrier.hashCode();
        return result;
    }

}
