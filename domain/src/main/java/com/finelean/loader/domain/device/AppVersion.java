/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.device;

import com.finelean.loader.domain.common.MetricKey;
import com.finelean.loader.domain.common.MetricKeyable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * This class represents the app_version table.
 *
 * @author sso
 */
@Entity(name="app_version")
@IdClass(MetricKey.class)
public class AppVersion implements MetricKeyable, Serializable {

    private static final long serialVersionUID = 100000004L;

    @EmbeddedId
    protected MetricKey metricKey;

    @Override
    public MetricKey getMetricKey() {
        return metricKey;
    }

    public void setMetricKey(MetricKey metricKey) {
        this.metricKey = metricKey;
    }

    @Column
    private Long counter;

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppVersion that = (AppVersion) o;

        if (!counter.equals(that.counter)) return false;
        if (!metricKey.equals(that.metricKey)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = metricKey.hashCode();
        result = 31 * result + counter.hashCode();
        return result;
    }
}
