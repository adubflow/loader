/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.device;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This class represents the device meta info.
 * Overloaded to bring info out from sqlite db.
 * When it comes to cassandra usage, the osVersion and the appVersion is used
 * in some other capacity.
 *
 * @author sso
 */
@Entity(name="device_meta_info")
@IdClass(DeviceMetaInfoKey.class)
public class DeviceMetaInfo implements Serializable {

    private static final long serialVersionUID = 100000000L;

    @EmbeddedId
    private DeviceMetaInfoKey deviceMetaInfoKey;

    @Column
    private String imsi;

    @Column(name="wifi_mac_address")
    private String wifiMacAddress;

    private String osVersion;

    private String appVersion;

    public DeviceMetaInfoKey getDeviceMetaInfoKey() {
        return deviceMetaInfoKey;
    }

    public void setDeviceMetaInfoKey(DeviceMetaInfoKey deviceMetaInfoKey) {
        this.deviceMetaInfoKey = deviceMetaInfoKey;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getWifiMacAddress() {
        return wifiMacAddress;
    }

    public void setWifiMacAddress(String wifiMacAddress) {
        this.wifiMacAddress = wifiMacAddress;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceMetaInfo that = (DeviceMetaInfo) o;

        if (!deviceMetaInfoKey.equals(that.deviceMetaInfoKey)) return false;
        if (!imsi.equals(that.imsi)) return false;
        if (!wifiMacAddress.equals(that.wifiMacAddress)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deviceMetaInfoKey.hashCode();
        result = 31 * result + imsi.hashCode();
        result = 31 * result + wifiMacAddress.hashCode();
        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
