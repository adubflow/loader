/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.event;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * This class models a type of event every 300 sec (5 minutes).
 * It represents the event counter table.
 *
 * @author sso
 */
@Entity(name="event_300")
@IdClass(EventByFiveMinuteKey.class)
public class EventByFiveMinute implements Serializable {

    private static final long serialVersionUID = 100000006L;

    @EmbeddedId
    private EventByFiveMinuteKey eventByFiveMinuteKey;

    @Column
    private Long counter;

    public EventByFiveMinuteKey getEventByFiveMinuteKey() {
        return eventByFiveMinuteKey;
    }

    public void setEventByFiveMinuteKey(EventByFiveMinuteKey eventByFiveMinuteKey) {
        this.eventByFiveMinuteKey = eventByFiveMinuteKey;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventByFiveMinute)) return false;

        EventByFiveMinute that = (EventByFiveMinute) o;

        if (!counter.equals(that.counter)) return false;
        if (!eventByFiveMinuteKey.equals(that.eventByFiveMinuteKey)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = eventByFiveMinuteKey.hashCode();
        result = 31 * result + counter.hashCode();
        return result;
    }
}
