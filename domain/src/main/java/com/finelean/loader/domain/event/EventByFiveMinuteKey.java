/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.event;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * This class represents the key for an event record every 5 minutes.
 *
 * @author sso
 */
public class EventByFiveMinuteKey implements Serializable {

    private static final long serialVersionUID = 100000007L;

    @Column(name="device_id")
    private String id;

    @Column(name="event_id")
    private Integer eventId;

    @Column(name="event_time_slot")
    private Date eventTimeSlot;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Date getEventTimeSlot() {
        return eventTimeSlot;
    }

    public void setEventTimeSlot(Date eventTimeSlot) {
        this.eventTimeSlot = eventTimeSlot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventByFiveMinuteKey)) return false;

        EventByFiveMinuteKey that = (EventByFiveMinuteKey) o;

        if (!eventId.equals(that.eventId)) return false;
        if (!eventTimeSlot.equals(that.eventTimeSlot)) return false;
        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + eventId.hashCode();
        result = 31 * result + eventTimeSlot.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "EventByFiveMinuteKey{" +
                "id='" + id + '\'' +
                ", eventId='" + eventId + '\'' +
                ", eventTimeSlot=" + eventTimeSlot +
                '}';
    }
}
