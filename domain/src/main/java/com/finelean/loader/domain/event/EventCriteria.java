package com.finelean.loader.domain.event;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by sso on 3/20/14.
 */
public class EventCriteria implements Serializable {

    private static final long serialVersionUID = 100000009L;

    private String name;

    private Date startTime;

    private Date endTime;

    private int threshold;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventCriteria)) return false;

        EventCriteria that = (EventCriteria) o;

        if (threshold != that.threshold) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + threshold;
        return result;
    }

    @Override
    public String toString() {
        return "EventCriteria{" +
                " name='" + name + '\'' +
                ", threshold=" + threshold +
                '}';
    }
}
