/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.device;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * This class represents the os version in the cassandra
 * @author sso
 */
@Entity(name="os_version")
@IdClass(OSVersionKey.class)
public class OSVersion implements Serializable {

    private static final long serialVersionUID = 100000002L;

    @EmbeddedId
    private OSVersionKey osVersionKey;

    @Column
    private Long counter;

    public OSVersionKey getOsVersionKey() {
        return osVersionKey;
    }

    public void setOsVersionKey(OSVersionKey osVersionKey) {
        this.osVersionKey = osVersionKey;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OSVersion osVersion = (OSVersion) o;

        if (!counter.equals(osVersion.counter)) return false;
        if (!osVersionKey.equals(osVersion.osVersionKey)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = osVersionKey.hashCode();
        result = 31 * result + counter.hashCode();
        return result;
    }
}
