/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.domain.event;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * This class represents an individual device event.
 *
 * @author sso
 */
@Entity(name="event")
@IdClass(EventKey.class)
public class Event implements Serializable {

    private static final long serialVersionUID = 100000005L;

    @EmbeddedId
    private EventKey eventKey;

    public EventKey getEventKey() {
        return eventKey;
    }

    public void setEventKey(EventKey eventKey) {
        this.eventKey = eventKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (!eventKey.equals(event.eventKey)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = eventKey.hashCode();

        return result;
    }

    @Override
    public String toString() {
        return String.format("event: %s", eventKey.toString());
    }
}
