package com.finelean.loader.domain.event;

import junit.framework.Assert;
import org.junit.Test;


/**
 * Created by sso on 3/18/14.
 */
public class TestEvent {

    @Test
    @SuppressWarnings("deprecation")
    public void testInterval() {

        Assert.assertTrue(doRound(1) == 0);
        Assert.assertTrue(doRound(4) == 0);
        Assert.assertTrue(doRound(5) == 5);
        Assert.assertTrue(doRound(8) == 5);
        Assert.assertTrue(doRound(10) == 10);
        Assert.assertTrue(doRound(12) == 10);
        Assert.assertTrue(doRound(15) == 15);
        Assert.assertTrue(doRound(19) == 15);
        Assert.assertTrue(doRound(20) == 20);

    }

    private int doRound(int unroundedMinutes) {

        return Math.round((unroundedMinutes / 5)) * 5;

    }
}
