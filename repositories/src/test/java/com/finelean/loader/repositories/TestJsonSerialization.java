package com.finelean.loader.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finelean.loader.domain.event.EventByFiveMinuteKey;
import com.finelean.loader.domain.event.EventCriteria;
import com.finelean.platform.util.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sso on 3/20/14.
 */
public class TestJsonSerialization {

    @Test
    public void testSerialization() throws Exception {

        Map<Integer, EventCriteria> result = new ObjectMapper().readValue(
                ClassLoader.getSystemResourceAsStream("eventCriteriaMap.json"), HashMap.class);

        Assert.assertTrue(result != null);
    }

    @Test
    public void testJsonObject() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper().setDateFormat(DateUtils.getLongFormat());

        EventByFiveMinuteKey key = objectMapper.readValue(
                ClassLoader.getSystemResourceAsStream("testPayload.json"),
                EventByFiveMinuteKey.class);

        Assert.assertTrue(key != null);
        //System.out.println(key);

    }

}
