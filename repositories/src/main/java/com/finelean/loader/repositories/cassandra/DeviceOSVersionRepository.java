/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.repositories.cassandra;

import com.finelean.loader.domain.device.DeviceMetaInfo;
import com.finelean.loader.domain.device.DeviceMetaInfoKey;
import com.finelean.loader.domain.device.OSVersion;
import com.finelean.loader.domain.device.OSVersionKey;
import org.easycassandra.persistence.cassandra.BatchBuilder;
import org.easycassandra.persistence.cassandra.UpdateBuilder;
import org.easycassandra.persistence.cassandra.spring.CassandraRepository;
import org.easycassandra.persistence.cassandra.spring.CassandraTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.finelean.platform.common.Constants.BLANK;
import static com.finelean.platform.common.Constants.MAKE_BIT_PATTERN;
import static com.finelean.platform.common.Constants.MODEL_BIT_PATTERN;

/**
 * Applying the technique of bit map index.
 *
 * http://www.slideshare.net/DataStax/cassandra-community-webinar-become-a-super-modeler
 *
 * @author sso
 */
@Repository("deviceOSVersionRepository")
public class DeviceOSVersionRepository extends CassandraRepository<OSVersion, OSVersionKey> {

    @Autowired
    private CassandraTemplate deviceCassandraTemplate;

    @Override
    public CassandraTemplate getCassandraTemplate() {
        return deviceCassandraTemplate;
    }


    public void persist(final DeviceMetaInfo deviceMetaInfo) {

        final BatchBuilder batchBuilder = deviceCassandraTemplate.batchBuilder();

        List<UpdateBuilder<OSVersion>> list = new ArrayList<>();

        DeviceMetaInfoKey key = deviceMetaInfo.getDeviceMetaInfoKey();

        // 3 combos only
        for (int i = 1; i < 4; i++) {

            UpdateBuilder<OSVersion> builder = deviceCassandraTemplate.updateBuilder(OSVersion.class);

            builder.whereEq("os_version", deviceMetaInfo.getOsVersion())
                    .whereEq("make", ((i & MAKE_BIT_PATTERN) == MAKE_BIT_PATTERN) ? key.getMake() : BLANK)
                    .whereEq("model", ((i & MODEL_BIT_PATTERN) == MODEL_BIT_PATTERN) ? key.getModel() : BLANK)
                    .incr("counter");

            list.add(builder);
        }

        for (UpdateBuilder<OSVersion> builder : list) {
            batchBuilder.addOperations(builder);
        }

        batchBuilder.execute();
    }

}
