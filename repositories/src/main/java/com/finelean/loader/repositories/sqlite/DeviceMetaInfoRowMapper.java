/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.repositories.sqlite;

import com.finelean.loader.domain.device.DeviceMetaInfo;
import com.finelean.loader.domain.device.DeviceMetaInfoKey;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.StringUtils;
import static com.finelean.platform.common.Constants.BLANK;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps a row from sqlite to POJO before going into Cassandra.
 *
 * @author sso
 */
public class DeviceMetaInfoRowMapper implements RowMapper<DeviceMetaInfo> {

    @Override
    public DeviceMetaInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        DeviceMetaInfoKey deviceMetaInfoKey = new DeviceMetaInfoKey();
        DeviceMetaInfo deviceMetaInfo = new DeviceMetaInfo();

        deviceMetaInfoKey.setId(rs.getString("deviceExternalId"));
        String str = rs.getString("devicemake");
        str = StringUtils.isEmpty(str) ? BLANK : str.toLowerCase();
        deviceMetaInfoKey.setMake(str);
        str = rs.getString("devicemodel");
        str = StringUtils.isEmpty(str) ? BLANK : str.toLowerCase();
        deviceMetaInfoKey.setModel(str);
        str = rs.getString("sim_carrier");
        str = StringUtils.isEmpty(str) ? BLANK : str.toLowerCase();
        deviceMetaInfoKey.setCarrier(str);

        deviceMetaInfo.setDeviceMetaInfoKey(deviceMetaInfoKey);

        deviceMetaInfo.setImsi(rs.getString("imsi"));
        deviceMetaInfo.setOsVersion(rs.getString("osVersion"));
        deviceMetaInfo.setAppVersion(rs.getString("client_version"));
        deviceMetaInfo.setWifiMacAddress(rs.getString("wifimacaddress"));

        return deviceMetaInfo;
    }
}
