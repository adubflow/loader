/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.repositories.cassandra;

import com.finelean.loader.domain.event.EventByFiveMinute;
import com.finelean.loader.domain.event.EventByFiveMinuteKey;
import org.easycassandra.persistence.cassandra.SelectBuilder;
import org.easycassandra.persistence.cassandra.UpdateBuilder;
import org.easycassandra.persistence.cassandra.spring.CassandraRepository;
import org.easycassandra.persistence.cassandra.spring.CassandraTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * This is the repository class for accessing the cassandra column family event_300.
 *
 * @author sso
 */
@Repository("eventByFiveMinuteRepository")
public class EventByFiveMinuteRepository extends CassandraRepository<EventByFiveMinute, EventByFiveMinuteKey> {

    private static final String COL_COUNTER = "counter";

    @Autowired
    private CassandraTemplate eventCassandraTemplate;

    @Override
    public CassandraTemplate getCassandraTemplate() {
        return eventCassandraTemplate;
    }

    public List<EventByFiveMinute> findByEventTimeBetween(final EventByFiveMinuteKey eventByFiveMinuteKey,
                                                          final Date startTime,
                                                          final Date endTime) {

        SelectBuilder<EventByFiveMinute> selectBuilder = eventCassandraTemplate.selectBuilder(EventByFiveMinute.class);

        List<EventByFiveMinute> list =
                selectBuilder.eq("device_id", eventByFiveMinuteKey.getId())
                            .eq("event_id", eventByFiveMinuteKey.getEventId())
                            .gte("event_time_slot", startTime)
                            .lt("event_time_slot", endTime)
                            .execute();

        return list;
    }

    // parameterize type later
    public void increment(EventByFiveMinute e) {

        EventByFiveMinuteKey key = e.getEventByFiveMinuteKey();

        final UpdateBuilder<EventByFiveMinute> builder =
                eventCassandraTemplate.updateBuilder(EventByFiveMinute.class, key);

        builder.incr(COL_COUNTER).execute();

    }
}
