/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.repositories.cassandra;

import com.finelean.loader.domain.device.AppVersion;
import com.finelean.loader.domain.device.DeviceMetaInfo;
import com.finelean.loader.domain.common.MetricKey;
import org.easycassandra.persistence.cassandra.spring.CassandraTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;

/**
 * This class provides the repository access to app version table
 * in device keyspace in cassandra.
 * The keyspace definition is in the scripts directory.
 *
 * @author sso
 */
@Repository("appVersionRepository")
public class AppVersionRepository extends AbstractRepositoryForCounter<AppVersion> {

    @Autowired
    private CassandraTemplate deviceCassandraTemplate;

    @PostConstruct
    public void init() {
        setCassandraTemplate(deviceCassandraTemplate);
    }

    public void persist(final DeviceMetaInfo deviceMetaInfo, final String date) {

        MetricKey metricKey = new MetricKey();
        metricKey.setId(deviceMetaInfo.getAppVersion());
        metricKey.setDate(date);

        AppVersion appVersion = new AppVersion();
        appVersion.setMetricKey(metricKey);

        increment(appVersion);
    }

}
