/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.repositories.task;

import com.finelean.loader.domain.device.DeviceMetaInfo;
import com.finelean.loader.domain.event.Event;
import com.finelean.loader.domain.event.EventByFiveMinute;
import com.finelean.loader.domain.event.EventByFiveMinuteKey;
import com.finelean.loader.domain.event.EventKey;
import com.finelean.loader.repositories.cassandra.*;
import com.finelean.loader.repositories.sqlite.QueryStatements;
import com.finelean.loader.repositories.sqlite.DeviceMetaInfoRowMapper;
import com.finelean.loader.transformer.DataTransformer;
import org.apache.log4j.Logger;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * A task to read the data from sqlite database to cassandra.
 *
 * @author sso
 */
@Component(LoadToRepositoryTask.NAME)
@Scope("prototype")
public class LoadToRepositoryTask implements Callable<Boolean> {

    private static final Logger logger = Logger.getLogger(LoadToRepositoryTask.class);
    private static final String DRIVER_NAME = "org.sqlite.JDBC";
    private static final String DRIVER_URL_PREFIX = "jdbc:sqlite:";
    private static final String EVENT_ID = "event_id";
    private static final String TIME = "time";
    private static final String EVENT_TABLE_NAME = "DCEvent2";
    public static final String NAME = "loadToRepositoryTask";

    private JdbcTemplate sqliteJDBCTemplate;

    private String date;

    private String path;

    @Autowired
    private DeviceMetaInfoRepository deviceMetaInfoRepository;

    @Autowired
    private AppVersionRepository appVersionRepository;

    @Autowired
    private DeviceOSVersionRepository deviceOSVersionRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventByFiveMinuteRepository eventByFiveMinuteRepository;

    @Autowired
    private DataTransformer dataTransformer;

    public LoadToRepositoryTask(final String path, final String date) {

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(DRIVER_NAME);

        final String url = DRIVER_URL_PREFIX + path;
        dataSource.setUrl(url);
        dataSource.setInitialSize(1);
        dataSource.setMaxActive(1);
        dataSource.setMaxIdle(1);
        dataSource.setPoolPreparedStatements(true);

        this.sqliteJDBCTemplate = new JdbcTemplate(dataSource);
        this.date = date;
        this.path = path;

    }

    /**
     * Persists the information into cassandra via the repository class.
     * Device id cannot be null - otherwise it cannot be identified in our eco-system.
     * In an ideal world, the device id should have been generated and assigned to the client.
     * But the device id generation and assignment is part of the registration process,
     * which is not the scope of this project.
     *
     *
     * @return true if loading is done.
     * @exception java.lang.IllegalArgumentException if the device id is empty.
     */
    @Override
    public Boolean call() {

        persist();

        // would have propagated exception from here on out
        return true;
    }

    private void persist() {

        final List<DeviceMetaInfo> deviceMetaInfos =
                sqliteJDBCTemplate.query(QueryStatements.DEVICE_METAINFO_QUERY, new DeviceMetaInfoRowMapper());

        // the query result says LIMIT 1 anyway
        if (deviceMetaInfos.isEmpty()) {

            throw new IllegalArgumentException(
                    String.format("Empty device info!  Date: %s; Path: %s", date, path));

        }

        DeviceMetaInfo d = deviceMetaInfos.get(0);

        String deviceId = d.getDeviceMetaInfoKey().getId();

        if (StringUtils.isEmpty(deviceId)) {

            throw new IllegalArgumentException(
                    String.format("device id must not be null.  Date: %s; Path: %s", date, path));

        }

        deviceMetaInfoRepository.persist(d);

        appVersionRepository.persist(d, date);

        deviceOSVersionRepository.persist(d);

        persistForEvent(deviceId);

    }

    private void persistForEvent(final String deviceId) {

        if (tableExists(EVENT_TABLE_NAME)) {

            sqliteJDBCTemplate.query(
                    QueryStatements.DEVICE_EVENT_QUERY,
                    new RowCallbackHandler() {

                        @Override
                        public void processRow(ResultSet rs) throws SQLException {

                            boolean hasResultSet = rs.next();

                            while (hasResultSet) {

                                Integer eventId = rs.getInt(EVENT_ID);

                                if (isToBeAnalyzedEvent(eventId)) {

                                    Date timestamp = new Date(rs.getLong(TIME));

                                    EventKey eventKey = new EventKey();
                                    eventKey.setId(deviceId);
                                    eventKey.setEventId(eventId);
                                    eventKey.setEventTime(timestamp);

                                    Event event = eventRepository.findOne(eventKey);
                                    if (event != null) {

                                        if (logger.isDebugEnabled()) {
                                            // don't construct the string if no debug
                                            logger.debug("Duplicate data from data source.  Don't double count  " + event.toString());
                                        }

                                    } else {

                                        event = new Event();
                                        event.setEventKey(eventKey);

                                        EventByFiveMinuteKey eventByFiveMinuteKey = new EventByFiveMinuteKey();
                                        eventByFiveMinuteKey.setId(deviceId);
                                        eventByFiveMinuteKey.setEventId(eventId);
                                        eventByFiveMinuteKey.setEventTimeSlot(calculateTimeBucket(timestamp));

                                        EventByFiveMinute eventByFiveMinute = new EventByFiveMinute();
                                        eventByFiveMinute.setEventByFiveMinuteKey(eventByFiveMinuteKey);

                                        eventRepository.save(event);
                                        eventByFiveMinuteRepository.increment(eventByFiveMinute);

                                    }

                                }

                                hasResultSet = rs.next();

                            }
                        }
                    }
            );
        } else {
            // we don't keep statistics on missing dcevent2;
            // the client policy has it that no-wifi -> don't transmitt events table.
        }
    }

    private boolean tableExists(final String tableName) {

        return sqliteJDBCTemplate.query(
                QueryStatements.QUERY_SQL_FOR_TABLE_EXISTENCE,
                new PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps) throws SQLException {
                        ps.setString(1, tableName);
                    }
                },
                new ResultSetExtractor<Boolean>() {
                    @Override
                    public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
                        int retVal = 0;
                        if (rs.next()) {
                            retVal = rs.getInt(1);
                        }

                        return retVal > 0;
                    }
                });
    }

    private boolean isToBeAnalyzedEvent(Integer eventId) {
        return dataTransformer.isAnalyzableEvent(eventId);
    }

    private Date calculateTimeBucket(final Date timestamp) {
        return dataTransformer.getTimeBucket(timestamp);
    }
}

