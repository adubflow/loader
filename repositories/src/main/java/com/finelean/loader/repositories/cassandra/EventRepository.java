package com.finelean.loader.repositories.cassandra;

import com.finelean.loader.domain.event.Event;
import com.finelean.loader.domain.event.EventKey;
import org.easycassandra.persistence.cassandra.spring.CassandraRepository;
import org.easycassandra.persistence.cassandra.spring.CassandraTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by sso on 3/3/14.
 */
@Repository("eventRepository")
public class EventRepository extends CassandraRepository<Event, EventKey>{

    @Autowired
    private CassandraTemplate eventCassandraTemplate;

    @Override
    public CassandraTemplate getCassandraTemplate() {
        return eventCassandraTemplate;
    }

}
