/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.repositories.cassandra;

import com.finelean.loader.domain.common.MetricKey;
import com.finelean.loader.domain.common.MetricKeyable;
import org.easycassandra.persistence.cassandra.UpdateBuilder;
import org.easycassandra.persistence.cassandra.spring.CassandraRepository;
import org.easycassandra.persistence.cassandra.spring.CassandraTemplate;

import java.lang.reflect.ParameterizedType;

/**
 * This class abstracts out the behavior for counter operation
 * for cassandra.
 * Tables requiring counter operation will extend this class.
 *
 * @author sso
 */
abstract class AbstractRepositoryForCounter<T extends MetricKeyable>
        extends CassandraRepository<T, MetricKey> {

    private static final String COL_COUNTER = "counter";

    private Class<T> beanClass;

    @SuppressWarnings("unchecked")
    public AbstractRepositoryForCounter() {

        ParameterizedType genericType = (ParameterizedType) this.getClass()
                .getGenericSuperclass();
        this.beanClass = (Class<T>) genericType.getActualTypeArguments()[0];
    }

    private CassandraTemplate cassandraTemplate;

    @Override
    public CassandraTemplate getCassandraTemplate() {
        return cassandraTemplate;
    }

    public void setCassandraTemplate(final CassandraTemplate cassandraTemplate) {
        this.cassandraTemplate = cassandraTemplate;
    }

    /**
     * Increments a counter column.
     *
     * @param metric
     */
    public void increment(T metric) {

        MetricKey key = metric.getMetricKey();

        final UpdateBuilder<T> builder = cassandraTemplate.updateBuilder(beanClass, key);

        builder.incr(COL_COUNTER).execute();

    }
}
