/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.repositories.cassandra;

import com.finelean.loader.domain.device.DeviceMetaInfo;
import com.finelean.loader.domain.device.DeviceMetaInfoKey;

import org.easycassandra.persistence.cassandra.BatchBuilder;
import org.easycassandra.persistence.cassandra.InsertBuilder;
import org.easycassandra.persistence.cassandra.spring.CassandraRepository;
import org.easycassandra.persistence.cassandra.spring.CassandraTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.finelean.platform.common.Constants.MAKE_BIT_PATTERN;
import static com.finelean.platform.common.Constants.MODEL_BIT_PATTERN;
import static com.finelean.platform.common.Constants.CARRIER_BIT_PATTERN;
import static com.finelean.platform.common.Constants.BLANK;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the device meta info.
 *
 * @author sso
 */
@Repository("deviceMetaInfoRepository")
public class DeviceMetaInfoRepository extends CassandraRepository<DeviceMetaInfo, DeviceMetaInfoKey> {

    @Autowired
    private CassandraTemplate deviceCassandraTemplate;

    @Override
    public CassandraTemplate getCassandraTemplate() {
        return deviceCassandraTemplate;
    }

    /**
     * Persists the make - model - info.
     * Inserted with a batch of 7, which different combo of make-model-info
     * for the same device.
     * This is the concept of bitmap index.
     * http://www.slideshare.net/DataStax/cassandra-community-webinar-become-a-super-modeler
     *
     * @param deviceMetaInfo
     */
    public void persist(final DeviceMetaInfo deviceMetaInfo) {


        final BatchBuilder batchBuilder = deviceCassandraTemplate.batchBuilder();

        List<InsertBuilder<DeviceMetaInfo>> list = new ArrayList<>();

        DeviceMetaInfoKey key = deviceMetaInfo.getDeviceMetaInfoKey();

        // 7 combos only
        for (int i = 1; i < 8; i++) {

            InsertBuilder<DeviceMetaInfo> builder = deviceCassandraTemplate.insertBuilder(DeviceMetaInfo.class);

            builder.value("id", key.getId())
                    .value("make", ((i & MAKE_BIT_PATTERN) == MAKE_BIT_PATTERN) ? key.getMake() : BLANK)
                    .value("model", ((i & MODEL_BIT_PATTERN) == MODEL_BIT_PATTERN) ? key.getModel() : BLANK)
                    .value("carrier", ((i & CARRIER_BIT_PATTERN) == CARRIER_BIT_PATTERN) ? key.getCarrier() : BLANK)
                    .value("imsi", deviceMetaInfo.getImsi())
                    .value("wifi_mac_address", deviceMetaInfo.getWifiMacAddress());

            list.add(builder);
        }

        for (InsertBuilder<DeviceMetaInfo> builder : list) {
            batchBuilder.addOperations(builder);
        }

        batchBuilder.execute();

    }

}
