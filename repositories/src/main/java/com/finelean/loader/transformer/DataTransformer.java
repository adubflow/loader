/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.loader.transformer;

import com.finelean.loader.domain.event.EventCriteria;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * This class holds the methods for transforming the data, as in the ETL process.
 *
 * @author sso
 */
@Component
public class DataTransformer {

    public static final int FIVE_MINUTE_BUCKET_SIZE = 5;

    @Resource
    private Map<Integer, EventCriteria> eventCriteriaMap;

    public boolean isAnalyzableEvent(Integer eventId) {
        return eventCriteriaMap.containsKey(eventId);
    }


    @SuppressWarnings("deprecation")
    public Date getTimeBucket(Date timestamp) {

        Date retVal = (Date)timestamp.clone();

        int rounded = Math.round((timestamp.getMinutes() / FIVE_MINUTE_BUCKET_SIZE)) * FIVE_MINUTE_BUCKET_SIZE;

        retVal.setSeconds(0);

        retVal.setMinutes(rounded);

        return retVal;
    }


}
