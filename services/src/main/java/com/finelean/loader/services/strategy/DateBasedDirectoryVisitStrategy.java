/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.services.strategy;

import com.finelean.platform.util.DateUtils;

import java.util.Date;

/**
 * The directory visit strategy which checks the range of
 * directory input to determine whether this directory should
 * be visited or not.
 *
 * @author sso
 */
public class DateBasedDirectoryVisitStrategy implements DirectoryVisitStrategy {

    private Date fromDate;

    private Date toDate;

    public DateBasedDirectoryVisitStrategy(Date fromDate, Date toDate) {
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    /**
     *
     * @param dir a date based directory in the format of yyyy-MM-dd
     * @return true if the input falls in the range.
     */
    @Override
    public boolean canBeVisited(String dir) {

        Date date = DateUtils.convert(dir);

        return ((fromDate.compareTo(date) <= 0)
                && (date.compareTo(toDate) <= 0));
    }
}
