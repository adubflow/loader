/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.services.strategy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

/**
 * This class encapsulates the logic on distinguishing
 * whether a directory should be visited.
 *
 * @author sso
 */
public class HashBasedDirectoryVisitStrategy implements DirectoryVisitStrategy {

    private static final Logger logger = Logger.getLogger(HashBasedDirectoryVisitStrategy.class);

    private int hostHash;

    private int totalHosts;

    public HashBasedDirectoryVisitStrategy(int hostHash, int totalHosts) {
        this.hostHash = hostHash;
        this.totalHosts = totalHosts;
    }

    /**
     * Determines whether its subdirectories should be visited.
     * Using this to configure multiple machines running this process.
     * If machine_1 has host hash 1, out of total of 4 machines,
     * 10-01, 10-05, 10-09 directory will be processed.
     * 10-02, 03, 04 will not be processed by machine_1.
     *
     * @param date
     * @return true if the directory represented by that date can be visited.
     */
    @Override
    public boolean canBeVisited(final String date) {

        int len = date.length();

        // sub-directory.  move on
        if (len <= 2) return true;

        String dayString = date.substring(len - 2, len);

        Integer day = Integer.parseInt(dayString);

        final boolean canBeVisited = day % totalHosts == hostHash;

        if (canBeVisited) {

        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Skipped: at host: " + hostHash + " date: " + date);
            }
        }
        return canBeVisited;
    }
}
