/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.services.strategy;

/**
 * An interface for whether the input directory
 * can be visited.
 *
 * @author sso
 */
public interface DirectoryVisitStrategy {

    /**
     * Whether a directory should be visited.
     *
     * @param dir in the form of yyyy-MM-dd
     * @return null if not to be visited.
     */
    boolean canBeVisited(final String dir);
}
