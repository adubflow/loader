package com.finelean.loader.task;

import com.finelean.loader.domain.common.MetricKey;
import com.finelean.loader.domain.operation.*;
import com.finelean.loader.repositories.cassandra.*;
import com.finelean.platform.common.Constants;
import com.finelean.platform.util.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;

/**
 * The processor unit which handles what has been uploaded.
 * It will pass on the sqlite handling to ZipFileStructureWalker.
 *
 * @author sso
 */
@Component(ZipFileProcessor.NAME)
@Scope("prototype")
public class ZipFileProcessor implements Callable<String> {

    private static final Logger logger = Logger.getLogger(ZipFileProcessor.class);

    public static final String NAME = "zipFileProcessor";

    private String dateToBeScanned;

    private Path file;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ExtractSuccessRepository extractSuccessRepository;

    @Autowired
    private ExtractFailureRepository extractFailureRepository;

    @Autowired
    private UploadDuplicateRepository uploadDuplicateRepository;

    @Autowired
    private ExceptionLookupRepository exceptionLookupRepository;

    public ZipFileProcessor(Path file, String dateToBeScanned) {
        this.dateToBeScanned = dateToBeScanned;
        this.file = file;
    }

    @Override
    public String call() {

        try {

            // calculating MD5 checksum of the file content is too expensive.
            // use device id - size combo instead.
            final String deviceId = file.toString().split(Constants.FILE_EXT)[1];
            String md5Str = deviceId + "-" + Files.size(file);
            md5Str = DigestUtils.md5DigestAsHex(md5Str.getBytes());

            MetricKey metricKeyWithIdAndSize = new MetricKey();
            metricKeyWithIdAndSize.setDate(dateToBeScanned);
            metricKeyWithIdAndSize.setId(md5Str);

            DuplicatedUploads duplicatedUploads = uploadDuplicateRepository.findOne(metricKeyWithIdAndSize);

            if (duplicatedUploads != null) {

                duplicatedUploads.setMetricKey(metricKeyWithIdAndSize);

                /*
                logger.debug(String.format("Found duplicate! %s %s count: %d",
                        duplicatedUploads.getMetricKey().getDeviceId(),
                        duplicatedUploads.getMetricKey().getPayloadChecksum(),
                        duplicatedUploads.getCounter()));
                */

            } else {

                duplicatedUploads = new DuplicatedUploads();
                duplicatedUploads.setCounter(0L);
                duplicatedUploads.setMetricKey(metricKeyWithIdAndSize);

                MetricKey metricKey = new MetricKey();
                metricKey.setId(deviceId);
                metricKey.setDate(dateToBeScanned);

                try (FileSystem zipfs = FileSystems.newFileSystem(file, null)) {

                    final Path root = zipfs.getPath("/");

                    Files.walkFileTree(root, (ZipFileStructureWalker)
                            applicationContext.getBean(ZipFileStructureWalker.NAME));

                    ExtractSuccess extractSuccess = new ExtractSuccess();
                    extractSuccess.setMetricKey(metricKey);
                    extractSuccessRepository.increment(extractSuccess);

                } catch (Throwable zipe) {

                    logger.error(file.toString() + ": " + zipe.getMessage());

                    ExtractFailure extractFailure = new ExtractFailure();
                    extractFailure.setMetricKey(metricKey);
                    extractFailureRepository.increment(extractFailure);

                    ExceptionLookup exceptionLookup = new ExceptionLookup();
                    exceptionLookup.setMetricKey(metricKeyWithIdAndSize);
                    exceptionLookup.setExceptionStack(Utils.convertStackTrace(zipe));

                    exceptionLookupRepository.save(exceptionLookup);

                }

            }

            uploadDuplicateRepository.increment(duplicatedUploads);

        } catch (Exception e) {

            logger.error("Error: ", e);

        }

        return null;

    }
}
