package com.finelean.loader.task;

import com.finelean.loader.services.strategy.DirectoryVisitStrategy;
import com.finelean.loader.services.strategy.HashBasedDirectoryVisitStrategy;
import com.finelean.platform.util.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Visits a directory.
 * This bean cannot be wired right out.  It depends on the dateToBeScanned.
 *
 * @author sso
 */
@Component(DirectoryWalker.NAME)
@Scope("prototype")
public class DirectoryWalker extends SimpleFileVisitor<Path> {

    public static final String NAME = "directoryWalker";

    private static final Logger logger = Logger.getLogger(DirectoryWalker.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ThreadPoolTaskExecutor loadToRepositoryTaskExecutor;

    @Autowired
    private DirectoryVisitStrategy hashBasedDirectoryVisitStrategy;

    private List<DirectoryVisitStrategy> directoryVisitStrategies;

    private String dateToBeScanned;

    public void add(DirectoryVisitStrategy d) {
        directoryVisitStrategies.add(d);
    }

    public DirectoryWalker() {
        directoryVisitStrategies = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        directoryVisitStrategies.add(hashBasedDirectoryVisitStrategy);
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

        String datePart = Utils.extractDateString(dir.toString());

        if (datePart.length() > 2) {

            dateToBeScanned  = datePart;
            try {
                for (DirectoryVisitStrategy directoryVisitStrategy : directoryVisitStrategies) {
                    if (!directoryVisitStrategy.canBeVisited(dateToBeScanned)) {
                        return FileVisitResult.TERMINATE;
                    }
                }
            } catch (Exception e) {

                logger.error(dir.getFileName(), e);

                return FileVisitResult.TERMINATE;
            }
        }

        return super.preVisitDirectory(dir, attrs);
    }

    /**
     * Processes the zip file.
     *
     * @param path to the zip file
     * @param attrs attributes
     * @return file visit result.
     * @throws IOException
     */
    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {

        ZipFileProcessor zipFileProcessor = (ZipFileProcessor)
                applicationContext.getBean(ZipFileProcessor.NAME, path, dateToBeScanned);

        loadToRepositoryTaskExecutor.submit(zipFileProcessor);

        return FileVisitResult.CONTINUE;

    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

        logger.warn("Really?  We got to visit file failed?  ", exc);

        return super.visitFileFailed(file, exc);
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return super.postVisitDirectory(dir, exc);
    }
}
