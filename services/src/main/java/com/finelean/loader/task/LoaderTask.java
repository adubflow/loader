package com.finelean.loader.task;

import com.finelean.loader.services.strategy.DirectoryVisitStrategy;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.nio.file.*;
import java.util.concurrent.Callable;


/**
 *
 * @author sso
 */
@Component(LoaderTask.NAME)
@Scope("prototype")
public class LoaderTask implements Callable<Boolean> {

    private static final Logger logger = Logger.getLogger(LoaderTask.class);

    public static final String NAME = "loaderTask";
    private String path;
    private DirectoryVisitStrategy directoryVisitStrategy;

    @Autowired
    private ApplicationContext applicationContext;

    public LoaderTask(final String path, final DirectoryVisitStrategy directoryVisitStrategy) {
        this.path = path;
        this.directoryVisitStrategy = directoryVisitStrategy;
    }

    @Override
    public Boolean call() {

        Path start = FileSystems.getDefault().getPath(path);
        try {

            DirectoryWalker v = (DirectoryWalker)applicationContext.getBean(DirectoryWalker.NAME);
            if (directoryVisitStrategy != null) {
                v.add(directoryVisitStrategy);
            }
            Files.walkFileTree(start, v);

        } catch (Exception e) {
            logger.error("Exception: ", e);
        }

        return Boolean.TRUE;
    }
}
