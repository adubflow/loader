/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.task;

import com.finelean.loader.domain.common.MetricKey;
import com.finelean.loader.domain.operation.*;
import com.finelean.loader.repositories.cassandra.ExceptionLookupRepository;
import com.finelean.loader.repositories.cassandra.LoadFailureRepository;
import com.finelean.loader.repositories.cassandra.LoadSuccessRepository;
import com.finelean.loader.repositories.task.LoadToRepositoryTask;
import com.finelean.platform.util.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 *
 * @author sso
 */
@Component(ZipFileStructureWalker.NAME)
@Scope("prototype")
class ZipFileStructureWalker extends SimpleFileVisitor<Path> {

    private static final Logger logger = Logger.getLogger(ZipFileStructureWalker.class);

    private static final String SQLITE_EXT = ".sqlite";

    private static final String VAR_TMP = "/var/tmp";

    public static final String NAME = "zipFileStructureWalker";

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private LoadSuccessRepository loadSuccessRepository;

    @Autowired
    private LoadFailureRepository loadFailureRepository;

    @Autowired
    private ExceptionLookupRepository exceptionLookupRepository;

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            return super.preVisitDirectory(dir, attrs);
    }

    @Override
    public FileVisitResult visitFile(Path innerFile, BasicFileAttributes attrs) throws IOException {

        final String innerFileName = innerFile.getFileName().toString();
        boolean success;

        if (innerFileName.endsWith(SQLITE_EXT)) {

            Path tmpFile = FileSystems.getDefault().getPath(VAR_TMP, innerFileName);

            final String[] strings = innerFileName.split("\\.");
            String deviceId = strings[0];
            final String date = strings[1].substring(0, 8);

            MetricKey metricKey = new MetricKey();
            metricKey.setDate(date);
            metricKey.setId(deviceId);

            try {
                // there is no direct way to stream this out into DataSource, hence copying...
                Files.copy(innerFile, tmpFile, StandardCopyOption.REPLACE_EXISTING);

                LoadToRepositoryTask loadToRepositoryTask = (LoadToRepositoryTask)
                applicationContext.getBean(LoadToRepositoryTask.NAME, tmpFile.toString(), date);

                // no need to put in thread pool

                if (StringUtils.isEmpty(deviceId)) {
                    throw new Exception("null deviceid.");
                }

                success = loadToRepositoryTask.call();

                if (success) {
                    LoadSuccess loadSuccess = new LoadSuccess();
                    loadSuccess.setMetricKey(metricKey);
                    loadSuccessRepository.increment(loadSuccess);
                }

            } catch(Exception e) {

                logger.error(innerFileName, e);

                ExceptionLookup exceptionLookup = new ExceptionLookup();
                exceptionLookup.setExceptionStack(Utils.convertStackTrace(e));
                exceptionLookup.setMetricKey(metricKey);
                exceptionLookupRepository.save(exceptionLookup);

                LoadFailure loadFailure = new LoadFailure();
                loadFailure.setMetricKey(metricKey);
                loadFailureRepository.increment(loadFailure);
            }

            if (Files.exists(tmpFile)) {
                // to prevent duplicate entries being deleted by the first go.
                Files.delete(tmpFile);
            }

        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path innerFile, IOException exc) throws IOException {

        logger.error("serious?  Failed at visit file failed: ", exc);
        return super.visitFileFailed(innerFile, exc);

    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

        return super.postVisitDirectory(dir, exc);
    }

}