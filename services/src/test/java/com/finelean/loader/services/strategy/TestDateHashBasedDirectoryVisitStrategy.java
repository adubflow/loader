/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package com.finelean.loader.services.strategy;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by sso on 2/14/14.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= "classpath*:test-context.xml")
public class TestDateHashBasedDirectoryVisitStrategy {

    @BeforeClass
    public static void setSystemProps() {
        System.setProperty("FINELEAN_GLOBAL_CONFIG",
                ClassLoader.getSystemResource("test-config.properties").getPath());
    }

    @Autowired
    DirectoryVisitStrategy directoryVisitStrategy;

    @Test
    public void testDateHash() {
        // 2 % 4 = 2
        assert(directoryVisitStrategy.canBeVisited("2013-10-02"));
        // 6 % 4  = 2
        assert(directoryVisitStrategy.canBeVisited("2013-10-06"));
        // 14 % 4 = 2
        assert(directoryVisitStrategy.canBeVisited("2013-10-14"));


        assert(!directoryVisitStrategy.canBeVisited("2013-10-16"));

        assert(!directoryVisitStrategy.canBeVisited("2013-10-03"));

        assert(!directoryVisitStrategy.canBeVisited("2013-10-13"));

        assert(!directoryVisitStrategy.canBeVisited("2013-10-07"));
    }
}
