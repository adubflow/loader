# Introduction

This project is an implementation example on ETL with Cassandra:

* extracting zip files from a group of pre-existing directories
* loading data in sqlite files into a cassandra cluster.  

![Slide1.jpg](https://bitbucket.org/repo/4goXz4/images/1582922615-Slide1.jpg)

# Technology Stack

* JDK 7
* Spring 4
* Netty 4
* Cassandra (with DataStax java driver)

# Important Links

Jump directly to other parts of the document.

[System Details](https://bitbucket.org/adubflow/loader/wiki/System%20Details)

[Deployment](https://bitbucket.org/adubflow/loader/wiki/Deployment%20And%20Execution)

[System/Service Monitoring](https://bitbucket.org/adubflow/loader/wiki/The%20Server%20Operation%20Analysis)

[Cassandra Monitoring](https://bitbucket.org/adubflow/loader/wiki/Cassandra%20Operational%20Analysis)

# The What

My company's Android app, available on the GooglePlay, collected data in the range of 10k-100k devices *daily*.

We would like to understand:

1. What are the make + model + carrier distribution of our install base.  
2. Extract Success/Failure rate
3. Load Success/Failure rate.
4. Device events (wifi on/off, bluetooth on/off, etc) and their number of occurrences every day
5. The adoption ratio - did customer actually upgrade?
6. With the data, we can target a upgrade reminder!

# Build Instructions
```
#!shell
git clone https://adubflow@bitbucket.org/finelean/loader.git
cd loader
./gradlew zip
```
# Deployment And Execution
[Details](https://bitbucket.org/adubflow/loader/wiki/Deployment%20And%20Execution)

# Service Operation Monitoring and Analysis

Within a day, it has been loading 2 months worth of data.

[Details](https://bitbucket.org/adubflow/loader/wiki/The%20Problem%2C%20The%20Setup%2C%20The%20Result%2C%20and%20The%20Analysis) 

# Cassandra Operation Monitoring and Analysis

[Cassandra Analysis](https://bitbucket.org/adubflow/loader/wiki/Cassandra%20Operational%20Analysis)

# Cassandra Data Analysis

The ballpark figure.  Not exact figures for protecting the business interests.

* 50000+ unique devices successfully checked in
* samsung is the most popular make in our community
* A lot of people are still on Android 4.3.  The next group is Android 4.1.x
* High level of successful extraction
* High level of successful sqlite.   

# Roadmap

* More data models to answer more questions.  The data is there!
* Add a watcher service to "watch" the file upload from the directory of the current date.
* More unit tests
* Automate deployment.  Too manual!

# License

The loader is released under version 2.0 of the [Apache License](http://www.apache.org/licenses/LICENSE-2.0)